const HttpError = require('http-error-constructor');
const { catchErrors, sign, } = require('../utils');
const { User: userModel } = require('../models');

class UserLogin {
  create(req) {
    const { username, password } = req.body;
    return userModel.findByUsernameAndPassword(username, password)
      .then(doc => {
        if(!doc)
          throw new HttpError
            .NotFound('Username or password does not match.');

        const user = doc.toObject();
        delete user.password;
        // In a real world this must be async
        user.token = sign.createToken(user);
        return user;
      }).catch(catchErrors);
  }
}

module.exports = UserLogin;
