const HttpError = require('http-error-constructor');
const { catchErrors } = require('../utils');
const { Character: characterModel } = require('../models');

class Character {
  constructor() {
    this.middlewares = {
      all: [],
      find: [],
      get: [],
      create: [],
      update: [],
      delete: [],
    };
  }

  find(req) {
    return characterModel.find(req.query).catch(catchErrors);
  }

  get(req) {
    return characterModel.findById(req.params.id)
      .then(doc => {
        if(!doc)
          throw new HttpError
            .NotFound(`Cannot find a model with id ${req.params.id}`);

        return doc;
      }).catch(catchErrors);
  }

  create(req) {
    return characterModel.create(req.body).catch(catchErrors);
  }

  update(req) {
    return characterModel
      .findByIdAndUpdate(req.params.id, req.body, { new: true })
      .catch(catchErrors);
  }

  delete(req) {
    return characterModel.remove({ _id: req.params.id }).catch(catchErrors);
  }

  setup(app, path) {
    this.app = app;
    console.log('I can make things with this method');
  }
}

module.exports = Character;
