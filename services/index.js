const Characters = require('./characters');
const Users = require('./users');
const UserLogin = require('./userLogin');

module.exports = {
  Characters,
  Users,
  UserLogin,
};
