const HttpError = require('http-error-constructor');
const { catchErrors, sign, } = require('../utils');
const { User: userModel } = require('../models');

class Users {
  constructor() {
    this.middlewares = {
      all: [sign.jwtMiddleware],
      get: [(req, res, next) => {
        if(req.params.id === 'me')
          req.params.id = req.user._id;

        return next();
      }],
    };
  }

  find(req) {
    return userModel.find(req.query).catch(catchErrors);
  }

  get(req) {
    const filters = req.query.filters;
    const query = userModel.findById(req.params.id);

    // put this logic in another part for reuse
    if(filters) {
      if(filters.populate) {
        query.populate(filters.populate);
      }
      if(filters.select) {
        if(Array.isArray(filters.select)) {
          const fields = {};
          Object.keys(filters.select).forEach(k => (fields[k] = 1));
          query.select(fields);
        } else if(
          typeof filters.select === 'string' ||
          typeof filters.select === 'object'
        ) {
          query.select(filters.select);
        }
      }
      // more filter handles
    }
    return query.exec().then(doc => {
      if(!doc)
        throw new HttpError
          .NotFound(`Cannot find a model with id ${req.params.id}`);

      const user = doc.toObject();
      delete user.password;

      return user;
    }).catch(catchErrors);
  }

  create(req) {
    return userModel.create(req.body).catch(catchErrors);
  }

  update(req) {
    return userModel
      .findByIdAndUpdate(req.params.id, req.body, { new: true })
      .catch(catchErrors);
  }

  delete(req) {
    return userModel.remove({ _id: req.params.id }).catch(catchErrors);
  }

  setup(app) {
    this.app = app;
  }
}

module.exports = Users;
