'use strict';

const bodyParser = require('body-parser');
const helmet = require('helmet');
const cors = require('cors');
const createApplication = require('./app/index');
const {
  Characters,
  Users,
  UserLogin,
} = require('./services');
const initConnection = require('./boot');
const {
  errorHandler,
  notFoundHandler,
  formatMessage
} = require('./utils');

const port = process.env.PORT || 3001;
const env = process.env.NODE_ENV || 'development';

const api = createApplication();
const app = createApplication();
api.use('/api/v1', app);

app.set('port', port);
app.set('env', env);
app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

initConnection(app);

app.setService('/characters', new Characters());
app.setService('/users/login', new UserLogin());
app.setService('/users', new Users());

app.use(formatMessage());
// handle error not found and generic
app.use(notFoundHandler());
app.use(errorHandler());

api.listen(port, () => console.log('app listening'));
