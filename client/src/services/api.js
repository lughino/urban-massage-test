// @flow
/* eslint-disable class-methods-use-this */
import axios from 'axios';
import type { CharacterType } from '../utils/types';

class ApiService {
  constructor() {
    this.api = axios.create({
      timeout: 10000,
      baseURL: 'http://localhost:3001/api/v1',
    });
    this.get = this.api.get;
    this.head = this.api.head;
    this.delete = this.api.delete;
    this.link = this.api.link;
    this.unlink = this.api.unlink;
    this.post = this.api.post;
    this.put = this.api.put;
    this.patch = this.api.patch;
  }

  getCharacters(): Promise<any> {
    return this.timeout(1000)
      .then((): Promise<any> => this.get('/characters'))
      .then((res: Object): Array<Object> => res.data)
      .catch(this.handleError);
  }

  createCharacter(character: CharacterType): Promise<any> {
    return this.post('/characters', character)
      .then((res: Object): Object => res.data)
      .catch(this.handleError);
  }

  updateCharacter(id: string, character: CharacterType): Promise<any> {
    return this.put(`/characters/${id}`, character)
      .then((res: Object): Object => res.data)
      .catch(this.handleError);
  }

  deleteCharacter(id: string): Promise<any> {
    return this.delete(`/characters/${id}`)
      .then((res: Object): Object => res.data)
      .catch(this.handleError);
  }

  getFavourites(id: string): Promise<any> {
    return this.get(`/users/${id}`, {
      params: {
        filters: {
          populate: 'favourites',
          select: 'favourites',
        }
      }
    }).then((res: Object): Array<Object> => res.data)
      .catch(this.handleError);
  }

  login(username: string, password: string): Promise<any> {
    return this.post('/users/login', { username, password })
      .then((res: Object): Object => res.data)
      .catch(this.handleError);
  }

  loginCheck(): Promise<any> {
    return this.get('/users/me')
      .then((res: Object): Object => res.data)
      .catch(this.handleError);
  }

  updateUser(id: string, data: Object): Promise<any> {
    return this.put(`/users/${id}`, data)
      .then((res: Object): Object => res.data)
      .catch(this.handleError);
  }

  addTokenToHeader(token: string) {
    this.api.defaults.headers.common.Authorization = `Bearer ${token}`;
  }

  removeTokenToHeader() {
    delete this.api.defaults.headers.common.Authorization;
  }

  handleError(e: Object): Object {
    let error = {};
    if(e.response) {
      error.message = e.response.statusText;
      error.data = e.response.data;
      error.code = e.response.status || -1;
    } else {
      error = e;
    }

    return Promise.reject(error);
  }

  timeout(ms: number): Promise {
    return new Promise((resolve: Function): any => setTimeout(resolve, ms));
  }
}

export default new ApiService();
