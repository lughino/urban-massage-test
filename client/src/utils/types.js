// @flow

export type UserType = {
  _id: string,
  name: string,
  surname: string,
  username: string,
  favourites: Array<string>
};

export type CharacterType = {
  _id: string,
  name: string,
  height: number,
  mass: number,
  hair_color: string,
  skin_color: string,
  eye_color: string,
  birth_year: string,
  is_male: boolean
};
