// @flow
import { schema } from 'normalizr';

const characterSchema = new schema.Entity('characters', {}, { idAttribute: '_id' });

export default {
  character: characterSchema,
  characters: [characterSchema],
};
