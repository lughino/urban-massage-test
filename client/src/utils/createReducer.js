// @flow
export default function createReducer(initialState: Object,
                                      // eslint-disable-next-line indent
                                      handlers: Object): Object {
  return (state: Object = initialState, action: Object): Object => {
    if(handlers[action.type])
      return handlers[action.type](state, action);

    return state;
  };
}
