import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { LinkContainer } from 'react-router-bootstrap';
import {
  Navbar,
  Nav,
  NavItem,
  NavDropdown,
  MenuItem,
  Glyphicon,
} from 'react-bootstrap';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import onlyUpdateForKeys from 'recompose/onlyUpdateForKeys';
import { logout } from '../../store/auth/actions';
import { getIsLogged, getUser } from '../../store/auth/selectors';
import './NavBar.css';
import logo from './logo.svg';
import type { StoreType } from '../../store';
import type { UserType } from '../../utils/types';

type NavPropsType = {
  isLogged: boolean,
  user: UserType,
  logout: () => {}
};

class NavBar extends Component<any, NavPropsType, any> {
  props: NavPropsType;

  handleLogout = (): void => this.props.logout();

  renderLinks = (): React$Element<any> => {
    if(this.props.isLogged)
      return (
        <NavDropdown
          eventKey={3}
          title={`${this.props.user.name} ${this.props.user.surname}`}
          id="nav-dropdown"
        >
          <MenuItem onClick={this.handleLogout} eventKey={3.1}>
            <Glyphicon glyph="log-out" />
            &nbsp; Logout
          </MenuItem>
        </NavDropdown>
      );

    return (
      <LinkContainer to="/login">
        <NavItem eventKey={3}>
          <Glyphicon glyph="log-in" />
          &nbsp; Login
        </NavItem>
      </LinkContainer>
    );
  };
  render(): React$Element<any> {
    return (
      <Navbar fixedTop collapseOnSelect className="NavBar-container">
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">
              <img src={logo} className="NavBar-logo" alt="Urban Massage" />
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to="/">
              <NavItem eventKey={1}>
                <Glyphicon glyph="home" />
                &nbsp; Characters
              </NavItem>
            </LinkContainer>
            {this.props.isLogged &&
            <LinkContainer to="/favourites">
              <NavItem eventKey={1}>
                <Glyphicon glyph="heart" />
                &nbsp; Favourites
              </NavItem>
            </LinkContainer>}
          </Nav>
          <Nav pullRight>
            <LinkContainer to="/characters/new">
              <NavItem eventKey={2}>
                <Glyphicon glyph="plus-sign" />
                &nbsp; New Character
              </NavItem>
            </LinkContainer>
            {this.renderLinks()}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

const mapStateToProps = (state: StoreType): Object => ({
  isLogged: getIsLogged(state),
  user: getUser(state),
});

const mapDispatchToProps = {
  logout,
};

const enhance = compose(
  onlyUpdateForKeys([
    'isLogged',
    'user',
  ]),
  connect(mapStateToProps, mapDispatchToProps),
  pure,
);

export default enhance(NavBar);
