// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import { reduxForm } from 'redux-form';
import CharacterForm from '../../components/character-form';
import {
  getCharacterByProps,
} from '../../store/characters/selectors';
import type { CharacterType } from '../../utils/types';

type PropsCharType = {
  handleSubmit: () => {},
  pristine: boolean,
  submitting: boolean,
  reset: () => {},
  initialValues: CharacterType,
  id: string
};

const validate = (values: Object): Object => {
  const errors = {};
  const requiredMessage = 'This field is Required';
  if(!values.name) {
    errors.name = requiredMessage;
  }
  if(!values.height) {
    errors.height = requiredMessage;
  }
  if(!values.mass) {
    errors.mass = requiredMessage;
  }
  if(!values.hair_color) {
    errors.hair_color = requiredMessage;
  }
  if(!values.skin_color) {
    errors.skin_color = requiredMessage;
  }
  if(!values.eye_color) {
    errors.eye_color = requiredMessage;
  }
  if(!values.birth_year) {
    errors.birth_year = requiredMessage;
  }
  if(!values.is_male) {
    errors.is_male = requiredMessage;
  }
  return errors;
};

class EditCharacterForm extends Component<any, PropsCharType, any> {
  props: PropsCharType;

  render(): React$Element<any> {
    return (
      <CharacterForm {...this.props} />
    );
  }
}

const mapStateToProps = (state: StoreType, props: PropsCharType): Object => ({
  initialValues: getCharacterByProps(state, props),
});

const mapDispatchToProps = {};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: 'editCharacterForm',
    validate,
  }),
  pure,
);

export default enhance(EditCharacterForm);
