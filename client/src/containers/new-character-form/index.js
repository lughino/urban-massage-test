// @flow
import React, { Component } from 'react';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import { reduxForm } from 'redux-form';
import CharacterForm from '../../components/character-form';

type PropsCharType = {
  handleSubmit: () => {},
  pristine: boolean,
  submitting: boolean,
  reset: () => {},
  initialValues?: Object
};

const validate = (values: Object): Object => {
  const errors = {};
  const requiredMessage = 'This field is Required';
  if(!values.name) {
    errors.name = requiredMessage;
  }
  if(!values.height) {
    errors.height = requiredMessage;
  }
  if(!values.mass) {
    errors.mass = requiredMessage;
  }
  if(!values.hair_color) {
    errors.hair_color = requiredMessage;
  }
  if(!values.skin_color) {
    errors.skin_color = requiredMessage;
  }
  if(!values.eye_color) {
    errors.eye_color = requiredMessage;
  }
  if(!values.birth_year) {
    errors.birth_year = requiredMessage;
  }
  if(!values.is_male) {
    errors.is_male = requiredMessage;
  }
  return errors;
};

class NewCharacterForm extends Component<any, PropsCharType, any> {
  props: PropsCharType;

  render(): React$Element<any> {
    return (
      <CharacterForm {...this.props} />
    );
  }
}

const enhance = compose(
  reduxForm({
    form: 'newCharacterForm',
    validate,
  }),
  pure,
);

export default enhance(NewCharacterForm);
