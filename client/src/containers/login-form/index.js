// @flow
import React, { Component } from 'react';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import { Field, reduxForm } from 'redux-form';
import { Button, ButtonToolbar, } from 'react-bootstrap';
import InputSimple from '../../components/input-simple';

type LoginFormPropsType = {
  handleSubmit: () => {},
  pristine: boolean,
  submitting: boolean,
  reset: () => {}
};

const validate = (values: Object): Object => {
  const errors = {};
  const requiredMessage = 'This field is Required';
  if(!values.username) {
    errors.username = requiredMessage;
  }
  if(!values.password) {
    errors.password = requiredMessage;
  }

  return errors;
};

class LoginForm extends Component<any, LoginFormPropsType, any> {
  props: LoginFormPropsType;

  render(): React$Element<any> {
    const { handleSubmit, pristine, reset, submitting, } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <Field
          name="username"
          type="text"
          component={InputSimple}
          label="UserName"
          value="username"
        />
        <Field
          name="password"
          type="password"
          component={InputSimple}
          label="Password"
          value="p4ssw0rd"
        />
        <ButtonToolbar>
          <Button
            bsStyle="primary"
            bsSize="large"
            type="submit"
            disabled={submitting}
          >
            Submit
          </Button>
          <Button
            bsStyle="default"
            bsSize="large"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Reset
          </Button>
        </ButtonToolbar>
      </form>
    );
  }
}

const enhance = compose(
  reduxForm({
    form: 'loginForm',
    validate,
  }),
  pure,
);

export default enhance(LoginForm);
