// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import onlyUpdateForKeys from 'recompose/onlyUpdateForKeys';
import {
  fetchCharacters,
  deleteCharacter,
} from '../../store/characters/actions';
import {
  getCharacters,
  getIsFetching,
  getCharacter,
} from '../../store/characters/selectors';
import PageContainer from '../../components/page-container';
import Modal from '../../components/modal';
import CharacterCard from '../character-card';
import type { CharacterType } from '../../utils/types';
import type { StoreType } from '../../store';

type PropsCharType = {
  isFetching: boolean,
  characterList: Array<CharacterType>,
  fetchCharacters: () => {},
  deleteCharacter: () => {},
  currentCharacter: CharacterType
};

type StateCharType = {
  showModal: boolean
};

class Characters extends Component<any, PropsCharType, StateCharType> {
  props: PropsCharType;

  state: StateCharType = {

  };

  componentDidMount() {
    this.props.fetchCharacters();
  }

  close = (): void => this.setState({ showModal: false });

  open = (): void => this.setState({ showModal: true });

  renderCharacter = (
    character: CharacterType,
    i: number,
  ): React$Element<any> => (
    <CharacterCard
      showActions
      key={`char${i}`}
      character={character}
      handleOpenModal={this.open}
    />
  );

  handleDelete = () => {
    const { currentCharacter } = this.props;
    this.props.deleteCharacter(currentCharacter._id);
    this.close();
  };

  render(): React$Element<any> {
    const { characterList, isFetching, } = this.props;
    return (
      <PageContainer
        title="Characters"
        isLoading={isFetching && !characterList.length}
      >
        {characterList.map(this.renderCharacter)}
        <Modal
          show={this.state.showModal}
          onHide={this.close}
          handlePress={this.handleDelete}
        />
      </PageContainer>
    );
  }
}

const mapStateToProps = (state: StoreType): Object => ({
  characterList: getCharacters(state),
  isFetching: getIsFetching(state),
  currentCharacter: getCharacter(state),
});

const mapDispatchToProps = {
  fetchCharacters,
  deleteCharacter,
};

const enhance = compose(
  onlyUpdateForKeys([
    'isFetching',
    'characterList',
    'currentCharacter',
  ]),
  connect(mapStateToProps, mapDispatchToProps),
  pure,
);

export default enhance(Characters);
