// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import { login } from '../../store/auth/actions';
import {
  getError,
  getIsFetching,
  getIsLogged,
} from '../../store/auth/selectors';
import PageContainer from '../../components/page-container';
import LoginForm from '../login-form';
import type { StoreType } from '../../store';

type LoginPropsType = {
  isFetching: boolean,
  isLogged: boolean,
  error: Object | null,
  login: () => {}
};
type LoginStateType = {
  showAlert: boolean,
  redirect: boolean
};

class Login extends Component<any, LoginPropsType, LoginStateType> {
  props: LoginPropsType;

  state: LoginStateType = {
    showAlert: false,
    redirect: false,
  };

  onSubmit = (e: Object): void => this.props.login(e);

  componentWillReceiveProps(
    nextProps: LoginPropsType
  ) {
    if(
      this.props.isFetching === true &&
      this.props.isFetching !== nextProps.isFetching &&
      nextProps.error == null
    ) {
      this.setState({ showAlert: true });
      if(!this.props.isLogged && nextProps.isLogged) {
        this.setState({ redirect: true });
      }
    }
  }

  renderAlert = (): React$Element<any> => {
    if(this.state.showAlert) {
      return (
        <Alert bsStyle={this.props.error ? 'error' : 'success'}>
          {this.props.error ?
            this.props.error.message || 'Error on login.' :
            'Login success.'
          }
        </Alert>
      );
    }
    return null;
  };

  render(): React$Element<any> {
    const { from } = this.props.location.state || { from: { pathname: '/' } };
    const { redirect } = this.state;

    if(redirect) {
      return (
        <Redirect to={from}/>
      );
    }

    return (
      <PageContainer
        title="Login"
      >
        <LoginForm onSubmit={this.onSubmit} />
        {this.renderAlert()}
      </PageContainer>
    );
  }
}

const mapStateToProps = (state: StoreType): Object => ({
  isFetching: getIsFetching(state),
  isLogged: getIsLogged(state),
  error: getError(state),
});

const mapDispatchToProps = {
  login,
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  pure,
);

export default enhance(Login);
