// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert } from 'react-bootstrap';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import { updateCharacter } from '../../store/characters/actions';
import {
  getError,
  getIsFetching,
} from '../../store/characters/selectors';
import PageContainer from '../../components/page-container';
import EditCharacterForm from '../edit-character-form';
import './EditCharacter.css';
import type { StoreType } from '../../store';

type EdChPrType = {
  isFetching: boolean,
  error: Object | null,
  updateCharacter: () => {}
};
type EditCharStateType = {
  showAlert: boolean
};

class EditCharacter extends Component<any, EdChPrType, EditCharStateType> {
  props: EdChPrType;

  state: EditCharStateType = {
    showAlert: false,
  };

  onSubmit = (e: Object): void => this.props.updateCharacter(e);

  componentWillReceiveProps(
    nextProps: EdChPrType
  ) {
    if(
      this.props.isFetching === true &&
      this.props.isFetching !== nextProps.isFetching &&
      nextProps.error == null
    ) {
      this.setState({ showAlert: true });
    }
  }

  renderAlert = (): React$Element<any> => {
    if(this.state.showAlert) {
      return (
        <Alert bsStyle={this.props.error ? 'error' : 'success'}>
          {this.props.error ?
            this.props.error.message || 'Generic error, try again.' :
            'Character modified.'
          }
        </Alert>
      );
    }
    return null;
  };

  render(): React$Element<any> {
    return (
      <PageContainer
        title="Edit Character"
      >
        <EditCharacterForm
          id={this.props.match.params.id}
          onSubmit={this.onSubmit}
        />
        {this.renderAlert()}
      </PageContainer>
    );
  }
}

const mapStateToProps = (state: StoreType): Object => ({
  isFetching: getIsFetching(state),
  error: getError(state),
});

const mapDispatchToProps = {
  updateCharacter,
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  pure,
);

export default enhance(EditCharacter);
