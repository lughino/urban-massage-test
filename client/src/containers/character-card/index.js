// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Col, Button, Glyphicon, ButtonToolbar } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import onlyUpdateForKeys from 'recompose/onlyUpdateForKeys';
import {
  selectCharacter,
} from '../../store/characters/actions';
import {
  toggleFavourite,
} from '../../store/auth/actions';
import { getIsLogged, getUser } from '../../store/auth/selectors';
import './CharacterCard.css';
import type { CharacterType, UserType, } from '../../utils/types';
import type { StoreType } from '../../store';

type CardPropsType = {
  isLogged: boolean,
  showActions: boolean,
  user: UserType,
  character: CharacterType,
  selectCharacter: () => {},
  toggleFavourite: () => {},
  handleOpenModal: () => {}
};

class CharacterCard extends Component<any, CardPropsType, any> {
  props: CardPropsType;

  isFavourite = (): string => {
    const { user, character } = this.props;
    return user.favourites.includes(character._id) ?
      'CharacterCard-btn-selected active' : '';
  };

  handleDelete = () => {
    const { selectCharacter: selC, character, handleOpenModal } = this.props;
    selC(character._id);
    handleOpenModal();
  };

  handleFavourite = () => {
    const { character, toggleFavourite: toggleF } = this.props;
    toggleF(character._id);
  };

  render(): React$Element<any> {
    const { character, isLogged, showActions } = this.props;
    return (
      <Col lg={4} md={6} sm={12}>
        <div className="CharacterCard-container">
          <div className="CharacterCard-title">
            <h3>{character.name}</h3>
          </div>
          <div className="CharacterCard-desc">
            <dl className="dl-horizontal">
              <dt>Birth Year</dt>
              <dd>{character.birth_year}</dd>
              <dt>Height</dt>
              <dd>{character.height}</dd>
              <dt>Mass</dt>
              <dd>{character.mass}</dd>
              <dt>Eye Color</dt>
              <dd>{character.eye_color}</dd>
              <dt>Hair Color</dt>
              <dd>{character.hair_color}</dd>
              <dt>Skin Color</dt>
              <dd>{character.skin_color}</dd>
              <dt>Sex</dt>
              <dd>{character.is_male}</dd>
            </dl>
            <div className="CharacterCard-actions">
              <ButtonToolbar className="CharacterCard-actions-toolbar">
                {showActions &&
                <LinkContainer
                  to={`/characters/${character._id}/edit`}
                  className="CharacterCard-btn-edit"
                >
                  <Button
                    bsStyle="warning"
                    className="CharacterCard-btn-edit"
                  >
                    <Glyphicon glyph="edit" />
                  </Button>
                </LinkContainer>}
                {isLogged &&
                <Button
                  bsStyle="info"
                  onClick={this.handleFavourite}
                  className={
                    `CharacterCard-btn-favourite ${this.isFavourite()}`
                  }
                >
                  <Glyphicon glyph="heart" />
                </Button>}
                {showActions &&
                <Button
                  bsStyle="danger"
                  className="CharacterCard-btn-remove"
                  onClick={this.handleDelete}
                >
                  <Glyphicon glyph="trash" />
                </Button>}
              </ButtonToolbar>
            </div>
          </div>
        </div>
      </Col>
    );
  }
}

const mapStateToProps = (state: StoreType): Object => ({
  isLogged: getIsLogged(state),
  user: getUser(state),
});

const mapDispatchToProps = {
  selectCharacter,
  toggleFavourite,
};

const enhance = compose(
  onlyUpdateForKeys([
    'user',
    'isLogged',
    'character',
  ]),
  connect(mapStateToProps, mapDispatchToProps),
  pure,
);

export default enhance(CharacterCard);
