// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import onlyUpdateForKeys from 'recompose/onlyUpdateForKeys';
import {
  fetchFavourites,
} from '../../store/auth/actions';
import {
  getFavouriteList,
  getIsFetching,
} from '../../store/auth/selectors';
import PageContainer from '../../components/page-container';
import CharacterCard from '../character-card';
import type { CharacterType } from '../../utils/types';
import type { StoreType } from '../../store';

type PropsFavouritesType = {
  isFetching: boolean,
  favouriteList: Array<CharacterType>,
  fetchFavourites: () => {}
};

class Characters extends Component<any, PropsFavouritesType, any> {
  props: PropsCharType;

  componentDidMount() {
    this.props.fetchFavourites();
  }

  renderCharacter = (
    character: CharacterType,
    i: number,
  ): React$Element<any> => (
    <CharacterCard
      key={`char${i}`}
      character={character}
    />
  );

  render(): React$Element<any> {
    const { favouriteList, isFetching, } = this.props;
    return (
      <PageContainer
        title="Favourites"
        isLoading={isFetching && !favouriteList.length}
      >
        {favouriteList.map(this.renderCharacter)}
      </PageContainer>
    );
  }
}

const mapStateToProps = (state: StoreType): Object => ({
  favouriteList: getFavouriteList(state),
  isFetching: getIsFetching(state),
});

const mapDispatchToProps = {
  fetchFavourites,
};

const enhance = compose(
  onlyUpdateForKeys([
    'isFetching',
    'favouriteList',
  ]),
  connect(mapStateToProps, mapDispatchToProps),
  pure,
);

export default enhance(Characters);
