// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert } from 'react-bootstrap';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import { createCharacter } from '../../store/characters/actions';
import {
  getError,
  getIsFetching,
} from '../../store/characters/selectors';
import PageContainer from '../../components/page-container';
import NewCharacterForm from '../new-character-form';
import type { StoreType } from '../../store';

type NewCharPropsType = {
  isFetching: boolean,
  error: Object | null,
  createCharacter: () => {}
};
type NewCharStateType = {
  showAlert: boolean
};

class NewCharacter extends Component<any, NewCharPropsType, NewCharStateType> {
  props: NewCharPropsType;

  state: NewCharStateType = {
    showAlert: false,
  };

  onSubmit = (e: Object): void => this.props.createCharacter(e);

  componentWillReceiveProps(
    nextProps: NewCharPropsType
  ) {
    if(
      this.props.isFetching === true &&
      this.props.isFetching !== nextProps.isFetching &&
      nextProps.error == null
    ) {
      this.setState({ showAlert: true });
    }
  }

  renderAlert = (): React$Element<any> => {
    if(this.state.showAlert) {
      return (
        <Alert bsStyle={this.props.error ? 'error' : 'success'}>
          {this.props.error ?
            this.props.error.message || 'Generic error, try again.' :
            'Character created.'
          }
        </Alert>
      );
    }
    return null;
  };

  render(): React$Element<any> {
    return (
      <PageContainer
        title="Create New Character"
      >
        <NewCharacterForm onSubmit={this.onSubmit} />
        {this.renderAlert()}
      </PageContainer>
    );
  }
}

const mapStateToProps = (state: StoreType): Object => ({
  isFetching: getIsFetching(state),
  error: getError(state),
});

const mapDispatchToProps = {
  createCharacter,
};

const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  pure,
);

export default enhance(NewCharacter);
