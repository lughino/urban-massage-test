// @flow
import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

// ACTIONS
import * as appAction from './app/actions';
import * as authAction from './auth/actions';
import * as charactersAction from './characters/actions';

// REDUCERS
import app from './app/reducer';
import auth from './auth/reducer';
import entities from './entities/reducer';
import characters from './characters/reducer';

// SAGAS
import appSaga from './app/sagas';
import characterSaga from './characters/sagas';
import authSaga, * as authSagas from './auth/sagas';

// SELECTORS
import * as appSelectors from './app/selectors';
import * as authSelectors from './auth/selectors';
import * as charactersSelectors from './characters/selectors';

// TYPES
import type { StateAppType } from './app/reducer';
import type { StateAuthType } from './auth/reducer';
import type { StateCharactersType } from './characters/reducer';

export const actions = {
  app: appAction,
  auth: authAction,
  characters: charactersAction,
};

export const selectors = {
  app: appSelectors,
  auth: authSelectors,
  characters: charactersSelectors,
};

export const sagas = {
  auth: authSagas,
};

export const rootReducer = combineReducers({
  app,
  auth,
  entities,
  form,
  characters,
});

export function* rootSaga(): void {
  yield [
    appSaga(),
    characterSaga(),
    authSaga(),
  ];
}

export type StoreType = StateAppType &
  StateAuthType &
  StateCharactersType;
