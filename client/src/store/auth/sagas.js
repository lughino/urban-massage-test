// @flow
import { put, apply, takeLatest, fork, select, } from 'redux-saga/effects';
import { normalize } from 'normalizr';
import {
  loginRequest,
  loginSuccess,
  loginError,
  loginCheckRequest,
  loginCheckSuccess,
  loginCheckError,
  logoutRequest,
  logoutSuccess,
  logoutError,
  toggleFavouriteRequest,
  toggleFavouriteSuccess,
  toggleFavouriteError,
  fetchFavouritesRequest,
  fetchFavouritesSuccess,
  fetchFavouritesError,
  LOGIN,
  LOGIN_CHECK,
  LOGOUT,
  CURRENT_USER_KEY,
  TOGGLE_FAVOURITE,
  FETCH_FAVOURITES
} from './actions';
import { getUser } from './selectors';
import api from '../../services/api';
import schemas from '../../utils/schema';

function* login(action: Object): void {
  yield put(loginRequest());
  const { username, password } = action.payload || {};
  try {
    const user = yield apply(api, api.login, [username, password]);
    api.addTokenToHeader(user.token);
    window.localStorage.setItem(CURRENT_USER_KEY, JSON.stringify(user));
    yield put(loginSuccess({ user, token: user.token }));
  } catch(e) {
    yield put(loginError(e));
  }
}

function* loginCheck(): void {
  yield put(loginCheckRequest());
  try {
    const user = yield apply(api, api.loginCheck, []);
    window.localStorage.setItem(CURRENT_USER_KEY, JSON.stringify(user));
    yield put(loginCheckSuccess({ user, token: user.token }));
  } catch(e) {
    yield put(loginCheckError(e));
  }
}

function* logout(): void {
  yield put(logoutRequest());
  try {
    // yield apply(api, api.logout, []);
    api.removeTokenToHeader();
    window.localStorage.removeItem(CURRENT_USER_KEY);
    yield put(logoutSuccess());
  } catch(e) {
    yield put(logoutError(e));
  }
}

function* toggleFavourite(action: Object): void {
  yield put(toggleFavouriteRequest());
  try {
    const id = action.payload;
    const user = yield select(getUser);
    const favourites = user.favourites.asMutable();
    const idx = favourites.indexOf(id);
    if(~idx)
      favourites.splice(idx, 1);
    else
      favourites.push(id);

    yield apply(api, api.updateUser, [user._id, { favourites }]);
    yield put(toggleFavouriteSuccess(favourites));
  } catch(e) {
    yield put(toggleFavouriteError(e));
  }
}

function* fetchFavourites(): void {
  yield put(fetchFavouritesRequest());
  try {
    const user = yield select(getUser);
    const data = yield apply(api, api.getFavourites, [user._id]);
    const { entities, result } = normalize(data.favourites, schemas.characters);
    yield put(fetchFavouritesSuccess(result, entities));
  } catch(e) {
    yield put(fetchFavouritesError(e));
  }
}

export function* initAuth() {
  const userString = window.localStorage.getItem(CURRENT_USER_KEY);
  if(userString) {
    try {
      const user = JSON.parse(userString);
      api.addTokenToHeader(user.token);
      yield fork(loginCheck);
    } catch(e) {
      console.log(e);
    }
  }
}

export default function* watch(): void {
  yield [
    takeLatest(LOGIN, login),
    takeLatest(LOGIN_CHECK, loginCheck),
    takeLatest(LOGOUT, logout),
    takeLatest(TOGGLE_FAVOURITE, toggleFavourite),
    takeLatest(FETCH_FAVOURITES, fetchFavourites),
  ];
}
