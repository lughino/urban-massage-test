// @flow
import Immutable from 'seamless-immutable';
import {
  LOGIN_CHECK_REQUEST,
  LOGIN_CHECK_SUCCESS,
  LOGIN_CHECK_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_ERROR,
  TOGGLE_FAVOURITE_REQUEST,
  TOGGLE_FAVOURITE_SUCCESS,
  TOGGLE_FAVOURITE_ERROR,
  FETCH_FAVOURITES_REQUEST,
  FETCH_FAVOURITES_SUCCESS,
  FETCH_FAVOURITES_ERROR,
} from './actions';
import createReducer from '../../utils/createReducer';
import type { UserType } from '../../utils/types';

export type StateAuthType = {
  isLogged: boolean,
  isFetching: boolean,
  user: UserType,
  error: null | Object,
  token: string | null
};
const initialState = Immutable({
  isLogged: false,
  isFetching: false,
  user: {
    favourites: [],
  },
  error: null,
  token: null,
});

const isFetching =
  (state: StateAuthType): StateAuthType =>
    state.merge({ isFetching: true });

const successLogin =
  (state: StateAuthType, action: Object): StateAuthType => {
    const { user, token } = action.payload;

    return state.merge({
      isFetching: false,
      isLogged: true,
      user,
      token,
      error: null,
    }, { deep: true });
  };

const successFavourites =
  (state: StateAuthType, action: Object): StateAuthType => {
    const favourites = action.payload;

    return state.merge({
      isFetching: false,
      user: {
        favourites,
      },
      error: null,
    }, { deep: true });
  };

const successLogout = (): StateAuthType => initialState;

const error =
  (state: StateAuthType, action: Object): StateAuthType =>
    state.merge({
      isFetching: false,
      error: action.payload
    });

export default createReducer(initialState, {
  [LOGIN_CHECK_REQUEST]: isFetching,
  [LOGIN_REQUEST]: isFetching,
  [LOGOUT_REQUEST]: isFetching,
  [LOGIN_SUCCESS]: successLogin,
  [LOGIN_ERROR]: error,
  [LOGIN_CHECK_SUCCESS]: successLogin,
  [LOGIN_CHECK_ERROR]: error,
  [LOGOUT_SUCCESS]: successLogout,
  [LOGOUT_ERROR]: error,
  [TOGGLE_FAVOURITE_REQUEST]: isFetching,
  [TOGGLE_FAVOURITE_SUCCESS]: successFavourites,
  [TOGGLE_FAVOURITE_ERROR]: error,
  [FETCH_FAVOURITES_REQUEST]: isFetching,
  [FETCH_FAVOURITES_SUCCESS]: successFavourites,
  [FETCH_FAVOURITES_ERROR]: error,
});
