// @flow
import { createSelector } from 'reselect';

export const getIsFetching = (state: Object): boolean =>
  state.auth.isFetching;

export const getIsLogged = (state: Object): boolean =>
  state.auth.isLogged;

export const getError = (state: Object): boolean => state.auth.error;

export const getAuth = (state: Object): string =>
  state.auth;

export const getFavourites = (state: Object): string =>
  state.auth.user.favourites;

export const getCharacterEntity = (state: Object): string =>
  state.entities.characters;

export const getUser = (state: Object): string =>
  state.auth.user;

export const getToken = (state: Object): string =>
  state.auth.user.token;

export const getFavouriteList = createSelector(
  getCharacterEntity,
  getFavourites,
  (characters: Object, favouriteList: Array<string>): Array<Object> =>
    favouriteList.map((f: string): Object => characters[f])
);
