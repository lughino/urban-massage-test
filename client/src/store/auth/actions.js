// @flow
import { createAction } from 'redux-actions';

export const LOGIN: string = 'LOGIN';
export const LOGIN_CHECK: string = 'LOGIN_CHECK';
export const LOGOUT: string = 'LOGOUT';
export const FETCH_FAVOURITES: string = 'FETCH_FAVOURITES';
export const TOGGLE_FAVOURITE: string = 'TOGGLE_FAVOURITE';
export const CURRENT_USER_KEY: string = 'CURRENT_USER_KEY';

export const LOGIN_REQUEST: string = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS: string = 'LOGIN_SUCCESS';
export const LOGIN_ERROR: string = 'LOGIN_ERROR';

export const LOGIN_CHECK_REQUEST: string = 'LOGIN_CHECK_REQUEST';
export const LOGIN_CHECK_SUCCESS: string = 'LOGIN_CHECK_SUCCESS';
export const LOGIN_CHECK_ERROR: string = 'LOGIN_CHECK_ERROR';

export const LOGOUT_REQUEST: string = 'LOGOUT_REQUEST';
export const LOGOUT_SUCCESS: string = 'LOGOUT_SUCCESS';
export const LOGOUT_ERROR: string = 'LOGOUT_ERROR';

export const TOGGLE_FAVOURITE_REQUEST: string = 'TOGGLE_FAVOURITE_REQUEST';
export const TOGGLE_FAVOURITE_SUCCESS: string = 'TOGGLE_FAVOURITE_SUCCESS';
export const TOGGLE_FAVOURITE_ERROR: string = 'TOGGLE_FAVOURITE_ERROR';

export const FETCH_FAVOURITES_REQUEST: string = 'FETCH_FAVOURITES_REQUEST';
export const FETCH_FAVOURITES_SUCCESS: string = 'FETCH_FAVOURITES_SUCCESS';
export const FETCH_FAVOURITES_ERROR: string = 'FETCH_FAVOURITES_ERROR';

export const login = createAction(LOGIN);
export const loginCheck = createAction(LOGIN_CHECK);
export const logout = createAction(LOGOUT);
export const fetchFavourites = createAction(FETCH_FAVOURITES);
export const toggleFavourite = createAction(TOGGLE_FAVOURITE);

export const loginRequest = createAction(LOGIN_REQUEST);
export const loginSuccess = createAction(LOGIN_SUCCESS);
export const loginError = createAction(LOGIN_ERROR);

export const loginCheckRequest = createAction(LOGIN_CHECK_REQUEST);
export const loginCheckSuccess = createAction(LOGIN_CHECK_SUCCESS);
export const loginCheckError = createAction(LOGIN_CHECK_ERROR);

export const logoutRequest = createAction(LOGOUT_REQUEST);
export const logoutSuccess = createAction(LOGOUT_SUCCESS);
export const logoutError = createAction(LOGOUT_ERROR);

export const toggleFavouriteRequest = createAction(TOGGLE_FAVOURITE_REQUEST);
export const toggleFavouriteSuccess = createAction(TOGGLE_FAVOURITE_SUCCESS);
export const toggleFavouriteError = createAction(TOGGLE_FAVOURITE_ERROR);

export const fetchFavouritesRequest = createAction(FETCH_FAVOURITES_REQUEST);
export const fetchFavouritesSuccess = createAction(
  FETCH_FAVOURITES_SUCCESS,
  undefined,
  (payload: Object, entities: Object): Object => ({ entities })
);
export const fetchFavouritesError = createAction(FETCH_FAVOURITES_ERROR);
