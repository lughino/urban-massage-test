// @flow
import Immutable from 'seamless-immutable';
import {
  CHARACTERS_REQUEST,
  CHARACTERS_ERROR,
  CHARACTERS_SUCCESS,
  CREATE_CHARACTER_REQUEST,
  CREATE_CHARACTER_SUCCESS,
  CREATE_CHARACTER_ERROR,
  UPDATE_CHARACTER_REQUEST,
  UPDATE_CHARACTER_SUCCESS,
  UPDATE_CHARACTER_ERROR,
  DELETE_CHARACTER_REQUEST,
  DELETE_CHARACTER_SUCCESS,
  DELETE_CHARACTER_ERROR,
  SELECT_CHARACTER,
} from './actions';
import createReducer from '../../utils/createReducer';

export type StateCharactersType = {
  isFetching: boolean,
  currentCharacter: string | null,
  characterList: Array<string>,
  error: null | Object
};
const initialState = Immutable({
  isFetching: false,
  currentCharacter: null,
  characterList: [],
  error: null
});

const isFetching =
  (state: StateCharactersType): StateCharactersType =>
    state.merge({ isFetching: true, error: null });

const success =
  (state: StateCharactersType, action: Object): StateCharactersType => {
    const { characterList } = action.payload;

    return state.merge({
      isFetching: false,
      characterList,
      error: null,
    }, { deep: true });
  };

const successDelete =
  (state: StateCharactersType, action: Object): StateCharactersType => {
    const { characterList } = action.payload;
    return state.merge({
      isFetching: false,
      error: null,
      characterList,
    });
  };

const error =
  (state: StateCharactersType, action: Object): StateCharactersType =>
    state.merge({
      isFetching: false,
      error: action.payload
    });

const setCurrentCharacter =
  (state: StateCharactersType, action: Object): StateCharactersType => {
    return state.merge({
      currentCharacter: action.payload
    });
  };

export default createReducer(initialState, {
  [CHARACTERS_REQUEST]: isFetching,
  [CREATE_CHARACTER_REQUEST]: isFetching,
  [UPDATE_CHARACTER_REQUEST]: isFetching,
  [DELETE_CHARACTER_REQUEST]: isFetching,
  [CHARACTERS_SUCCESS]: success,
  [CHARACTERS_ERROR]: error,
  [CREATE_CHARACTER_SUCCESS]: success,
  [UPDATE_CHARACTER_SUCCESS]: success,
  [DELETE_CHARACTER_SUCCESS]: successDelete,
  [CREATE_CHARACTER_ERROR]: error,
  [UPDATE_CHARACTER_ERROR]: error,
  [DELETE_CHARACTER_ERROR]: error,
  [SELECT_CHARACTER]: setCurrentCharacter,
});
