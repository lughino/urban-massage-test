// @flow
import { createAction } from 'redux-actions';

export const FETCH_CHARACTERS: string = 'FETCH_CHARACTERS';
export const CREATE_CHARACTER: string = 'CREATE_CHARACTERS';
export const UPDATE_CHARACTER: string = 'UPDATE_CHARACTERS';
export const DELETE_CHARACTER: string = 'DELETE_CHARACTERS';
export const SELECT_CHARACTER: string = 'SELECT_CHARACTERS';

export const CHARACTERS_REQUEST: string = 'CHARACTERS_REQUEST';
export const CHARACTERS_SUCCESS: string = 'CHARACTERS_SUCCESS';
export const CHARACTERS_ERROR: string = 'CHARACTERS_ERROR';

export const CREATE_CHARACTER_REQUEST: string = 'CREATE_CHARACTER_REQUEST';
export const CREATE_CHARACTER_SUCCESS: string = 'CREATE_CHARACTER_SUCCESS';
export const CREATE_CHARACTER_ERROR: string = 'CREATE_CHARACTER_ERROR';

export const UPDATE_CHARACTER_REQUEST: string = 'UPDATE_CHARACTER_REQUEST';
export const UPDATE_CHARACTER_SUCCESS: string = 'UPDATE_CHARACTER_SUCCESS';
export const UPDATE_CHARACTER_ERROR: string = 'UPDATE_CHARACTER_ERROR';

export const DELETE_CHARACTER_REQUEST: string = 'DELETE_CHARACTER_REQUEST';
export const DELETE_CHARACTER_SUCCESS: string = 'DELETE_CHARACTER_SUCCESS';
export const DELETE_CHARACTER_ERROR: string = 'DELETE_CHARACTER_ERROR';

export const fetchCharacters = createAction(FETCH_CHARACTERS);
export const createCharacter = createAction(CREATE_CHARACTER);
export const updateCharacter = createAction(UPDATE_CHARACTER);
export const deleteCharacter = createAction(DELETE_CHARACTER);

export const charactersRequest = createAction(CHARACTERS_REQUEST);
export const charactersSuccess = createAction(
  CHARACTERS_SUCCESS,
  undefined,
  (payload: Object, entities: Object): Object => ({ entities })
);
export const charactersError = createAction(CHARACTERS_ERROR);

export const createCharacterRequest = createAction(CREATE_CHARACTER_REQUEST);
export const createCharacterSuccess = createAction(
  CREATE_CHARACTER_SUCCESS,
  undefined,
  (payload: Object, entities: Object): Object => ({ entities })
);
export const createCharacterError = createAction(CREATE_CHARACTER_ERROR);

export const updateCharacterRequest = createAction(UPDATE_CHARACTER_REQUEST);
export const updateCharacterSuccess = createAction(
  UPDATE_CHARACTER_SUCCESS,
  undefined,
  (payload: Object, entities: Object): Object => ({ entities })
);
export const updateCharacterError = createAction(UPDATE_CHARACTER_ERROR);

export const deleteCharacterRequest = createAction(DELETE_CHARACTER_REQUEST);
export const deleteCharacterSuccess = createAction(DELETE_CHARACTER_SUCCESS);
export const deleteCharacterError = createAction(DELETE_CHARACTER_ERROR);

export const selectCharacter = createAction(SELECT_CHARACTER);
