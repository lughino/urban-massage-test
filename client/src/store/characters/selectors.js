// @flow
import { createSelector } from 'reselect';

export const getIsFetching = (state: Object): boolean =>
  state.characters.isFetching;

export const getIsRefreshing = (state: Object): boolean =>
  state.characters.isRefreshing;

export const getError = (state: Object): boolean => state.characters.error;

export const getCharactersEntity = (state: Object): Object =>
  state.entities.characters;

export const getCharacterByProps = (state: Object, props: Object): Object =>
  state.entities.characters[props.id];

export const getCharacter = (state: Object): Object =>
  state.entities.characters &&
  state.entities.characters[state.characters.currentCharacter];

export const getCharactersList = (state: Object): Object =>
  state.characters.characterList;

export const getCharacters = createSelector(
  getCharactersEntity,
  getCharactersList,
  (characters: Object, characterList: Object): Array<Object> =>
    characterList.map((c: string): Object => characters[c])
);
