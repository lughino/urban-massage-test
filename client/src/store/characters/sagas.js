// @flow
import { put, apply, select, takeLatest } from 'redux-saga/effects';
import { normalize } from 'normalizr';
import {
  charactersRequest,
  charactersSuccess,
  charactersError,
  createCharacterRequest,
  createCharacterSuccess,
  createCharacterError,
  updateCharacterRequest,
  updateCharacterSuccess,
  updateCharacterError,
  deleteCharacterRequest,
  deleteCharacterSuccess,
  deleteCharacterError,
  FETCH_CHARACTERS,
  CREATE_CHARACTER,
  UPDATE_CHARACTER,
  DELETE_CHARACTER,
} from './actions';
import { getCharactersList } from './selectors';
import api from '../../services/api';
import schemas from '../../utils/schema';

function* fetchCharacters(): void {
  yield put(charactersRequest());

  try {
    const data = yield apply(api, api.getCharacters, []);
    const { entities, result } = normalize(data, schemas.characters);
    yield put(charactersSuccess({ characterList: result }, entities));
  } catch(e) {
    yield put(charactersError(e));
  }
}

function* createCharacter(action: Object): void {
  yield put(createCharacterRequest());
  const character = action.payload;
  try {
    const data = yield apply(api, api.createCharacter, [character]);
    const { entities, result } = normalize(data, schemas.character);
    yield put(createCharacterSuccess({ characterList: [result] }, entities));
  } catch(e) {
    yield put(createCharacterError(e));
  }
}

function* updateCharacter(action: Object): void {
  yield put(updateCharacterRequest());
  const character = action.payload;
  try {
    const data = yield apply(api, api.updateCharacter,
      [character._id, character]);
    const { entities, result } = normalize(data, schemas.character);
    yield put(updateCharacterSuccess({ characterList: [result] }, entities));
  } catch(e) {
    yield put(updateCharacterError(e));
  }
}

function* deleteCharacter(action: Object): void {
  yield put(deleteCharacterRequest());
  const id = action.payload;
  try {
    yield apply(api, api.deleteCharacter, [id]);
    let characterList = yield select(getCharactersList);
    characterList = characterList.asMutable();
    const idx = characterList.indexOf(id);
    characterList.splice(idx, 1);
    yield put(deleteCharacterSuccess({ characterList }));
  } catch(e) {
    yield put(deleteCharacterError(e));
  }
}

export default function* watch(): void {
  yield [
    takeLatest(FETCH_CHARACTERS, fetchCharacters),
    takeLatest(CREATE_CHARACTER, createCharacter),
    takeLatest(UPDATE_CHARACTER, updateCharacter),
    takeLatest(DELETE_CHARACTER, deleteCharacter),
  ];
}
