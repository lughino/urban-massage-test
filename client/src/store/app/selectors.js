// @flow
import { createSelector } from 'reselect';

export const isLoaded = (state: Object): boolean => state.app.isLoaded;

export const getSettings = (state: Object): Object =>
  state.app.currentSettings;

export const getActiveSections = createSelector(
  getSettings,
  (settings: Object): Array<string> => Object.keys(settings)
      .filter((s: string): boolean => settings[s] === true)
      .map((s: string): boolean => s)
);
