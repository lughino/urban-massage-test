// @flow
import { createAction } from 'redux-actions';

export const INITIAL_STATE: string = 'INITIAL_STATE';
export const INITIAL_STATE_SUCCESS: string = 'INITIAL_STATE_SUCCESS';

export const initialState = createAction(INITIAL_STATE);
export const initialStateSuccess = createAction(INITIAL_STATE_SUCCESS);
