// @flow
import Immutable from 'seamless-immutable';
import { INITIAL_STATE_SUCCESS } from './actions';
import createReducer from '../../utils/createReducer';

export type StateAppType = {
  isLoaded: boolean,
  currentSettings: {
    [key: string]: boolean
  }
};
const initialState = Immutable({
  isLoaded: false,
  currentSettings: {
    Absences: true,
    Notes: true,
    Ratings: true,
    Interviews: true,
    Settings: true,
  },
});

export default createReducer(initialState, {
  [INITIAL_STATE_SUCCESS](state: StateAppType, action: Object): StateAppType {
    return state.merge({ isLoaded: true });
  }
});
