// @flow
import { put, call, takeLatest, } from 'redux-saga/effects';
import { initialStateSuccess, INITIAL_STATE } from './actions';
import { sagas, actions, } from '../';

export function* startup(): void {
  const isLogged = window.localStorage.getItem(actions.auth.CURRENT_USER_KEY);
  if(isLogged) {
    yield call(sagas.auth.initAuth);
  }
  yield put(initialStateSuccess());
}

export default function* watch(): void {
  yield takeLatest(INITIAL_STATE, startup);
}
