// @flow
import Immutable from 'seamless-immutable';
import type { CharacterType } from '../../utils/types';

export type StateEntitiesType = {
  characters: {
    [key: string]: CharacterType
  }
};
const initialState: StateEntitiesType = Immutable({});

export default function entities(state: StateEntitiesType = initialState,
                                 // eslint-disable-next-line indent
                                 action: any): StateEntitiesType {
  if(action.meta && action.meta.entities) {
    return state.merge(action.meta.entities, { deep: true });
  }
  return state;
}
