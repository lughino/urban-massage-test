// @flow
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import pure from 'recompose/pure';
import NoMatch from './components/no-match';
import Characters from './containers/characters';
import Favourites from './containers/favourites';
import NewCharacter from './containers/new-character';
import EditCharacter from './containers/edit-character';
import Login from './containers/login';
import NavBar from './containers/navbar';
import configureStore from './configureStore';
import { actions } from './store';

const store = configureStore();

class Root extends Component {
  componentDidMount() {
    store.dispatch(actions.app.initialState());
  }

  render(): React$Element<any> {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <NavBar />
            <Switch>
              <Route exact path="/" component={Characters}/>
              <Route path="/characters/new" component={NewCharacter}/>
              <Route path="/characters/:id/edit" component={EditCharacter}/>
              <Route path="/favourites" component={Favourites}/>
              <Route path="/login" component={Login}/>
              <Route component={NoMatch}/>
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default pure(Root);
