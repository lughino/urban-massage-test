// @flow
import { createStore, applyMiddleware, compose, } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { rootReducer, rootSaga } from './store';

export default function configureStore(): Object {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware];
  // eslint-disable-next-line no-underscore-dangle
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ||
    compose;
  const enhancer = composeEnhancers(
    applyMiddleware(...middlewares),
  );

  const store = createStore(
    rootReducer,
    enhancer,
  );
  sagaMiddleware.run(rootSaga);

  return store;
}
