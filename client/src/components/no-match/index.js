// @flow
import React, { Component } from 'react';
import { Grid, Col, Row, } from 'react-bootstrap';
import pure from 'recompose/pure';

class NoMatch extends Component {
  render() {
    return (
      <Grid>
        <Row>
          <Col lg={12} className="text-center">
            <h1>404</h1>
            <h2>Route not found</h2>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default pure(NoMatch);
