// @flow
import React, { Component } from 'react';
import {
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
} from 'react-bootstrap';
import pure from 'recompose/pure';

type InputPropsType = {
  controlId: string,
  label: string,
  type?: string,
  placeholder?: string,
  meta: {
    touched: boolean,
    error: string,
    warning: string
  }
};

class InputSimple extends Component<any, InputPropsType, any> {
  props: InputPropsType;

  render() {
    const {
      controlId,
      label,
      type,
      input,
      meta: { touched, error, }
    } = this.props;
    return (
      <FormGroup
        controlId={controlId}
        validationState={(touched && error) ? 'error' : null}
      >
        <ControlLabel>{label}</ControlLabel>
        <FormControl
          {...input}
          type={type || 'text'}
          placeholder={label}
        />
        <FormControl.Feedback />
        {touched && error && <HelpBlock>{error}</HelpBlock>}
      </FormGroup>
    );
  }
}

export default pure(InputSimple);
