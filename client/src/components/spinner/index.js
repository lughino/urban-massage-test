// @flow
import React, { Component } from 'react';
import onlyUpdateForKeys from 'recompose/onlyUpdateForKeys';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import './Spinner.css';

type SpinnerPropsType = {
  fullscreen: boolean
};

class Spinner extends Component<any, SpinnerPropsType, any> {
  props: SpinnerPropsType;

  static defaultProps = {
    fullscreen: true,
  };

  render() {
    const fullscreen = this.props.fullscreen ? 'Spinner-fullscreen' : '';
    return (
      <div className={`Spinner-wrapper ${fullscreen}`}>
        <div className={'Spinner-spin'}></div>
      </div>
    );
  }
}

const enhance = compose(
  onlyUpdateForKeys([
    'fullscreen',
  ]),
  pure,
);

export default enhance(Spinner);
