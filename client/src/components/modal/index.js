// @flow
import React, { Component } from 'react';
import {
  Modal,
  Button,
} from 'react-bootstrap';
import pure from 'recompose/pure';

type ModalPropsType = {
  handlePress: () => {}
};

class MyModal extends Component<any, ModalPropsType, any> {
  props: ModalPropsType;

  render() {
    const {
      handlePress,
      onHide,
      show
    } = this.props;
    return (
      <Modal show={show} onHide={onHide}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Character</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure?
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={onHide}>Close</Button>
          <Button onClick={handlePress} bsStyle="danger">Apply</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default pure(MyModal);
