// @flow
import React, { Component } from 'react';
import { Grid, Col, Row, PageHeader, } from 'react-bootstrap';
import onlyUpdateForKeys from 'recompose/onlyUpdateForKeys';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import './PageContainer.css';
import { FullScreenSpinner } from '../../hoc';

type PageContPropsType = {
  isLoading: boolean,
  title: string,
  children: Array<React$Element>
};

class PageContainer extends Component<any, PageContPropsType, any> {
  props: PageContPropsType;

  render() {
    return (
      <Grid className="PageContainer">
        <Row>
          <Col lg={12}>
            <PageHeader>{this.props.title}</PageHeader>
          </Col>
        </Row>
        <Row>
          <Col lg={12}>
            {this.props.children}
          </Col>
        </Row>
      </Grid>
    );
  }
}

const enhance = compose(
  onlyUpdateForKeys([
    'isLoading',
    'title',
    'children',
  ]),
  pure,
  FullScreenSpinner(
    (props: PageContPropsType): boolean => props.isLoading,
  )
);

export default enhance(PageContainer);
