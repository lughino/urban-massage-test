// @flow
import React, { Component } from 'react';
import compose from 'recompose/compose';
import pure from 'recompose/pure';
import { Field, } from 'redux-form';
import { Button, ButtonToolbar, } from 'react-bootstrap';
import InputSimple from '../input-simple';

type PropsCharType = {
  handleSubmit: () => {},
  pristine: boolean,
  submitting: boolean,
  reset: () => {},
  initialValues?: Object
};

class CharacterForm extends Component<any, PropsCharType, any> {
  props: PropsCharType;

  render(): React$Element<any> {
    const { handleSubmit, pristine, reset, submitting, } = this.props;
    return (
      <form onSubmit={handleSubmit}>
        <Field
          name="name"
          type="text"
          component={InputSimple}
          label="Name"
        />
        <Field
          name="hair_color"
          type="text"
          component={InputSimple}
          label="Hair Color"
        />
        <Field
          name="skin_color"
          type="text"
          component={InputSimple}
          label="Skin Color"
        />
        <Field
          name="eye_color"
          type="text"
          component={InputSimple}
          label="Eye Color"
        />
        <Field
          name="birth_year"
          type="text"
          component={InputSimple}
          label="Birth Year"
        />
        <Field
          name="height"
          type="number"
          component={InputSimple}
          label="Height"
        />
        <Field
          name="mass"
          type="number"
          component={InputSimple}
          label="Mass"
        />
        <div>
          <label>Sex</label>
          <div>
            <label>
              <Field
                name="is_male"
                component="input"
                type="radio"
                value={'true'}
              />
              &nbsp; Male
            </label>
            {'  '}
            <label>
              <Field
                name="is_male"
                component="input"
                type="radio"
                value={'false'}
              />
              &nbsp; Female
            </label>
          </div>
        </div>
        <ButtonToolbar>
          <Button
            bsStyle="primary"
            bsSize="large"
            type="submit"
            disabled={submitting}
          >
            Submit
          </Button>
          <Button
            bsStyle="default"
            bsSize="large"
            disabled={pristine || submitting}
            onClick={reset}
          >
            Reset
          </Button>
        </ButtonToolbar>
      </form>
    );
  }
}

const enhance = compose(
  pure,
);

export default enhance(CharacterForm);
