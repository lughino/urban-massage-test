// @flow
import branch from 'recompose/branch';
import renderComponent from 'recompose/renderComponent';
import Spinner from '../components/spinner';

export default (isLoading: boolean): React$Element =>
  branch(
    isLoading,
    renderComponent(Spinner),
  );
