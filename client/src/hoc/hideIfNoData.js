// @flow
import branch from 'recompose/branch';
import renderNothing from 'recompose/renderNothing';

export default (hasNoData: Function): Function =>
  branch(
    hasNoData,
    renderNothing,
  );
