const mongoose = require('mongoose');
const fixtures = require('./fixtures');

const mongoHost = process.env.MONGO_HOST || 'localhost';
const mongoPort = process.env.MONGO_PORT || 27017;
const mongoDb = process.env.MONGO_DB || 'urbanMassage';

module.exports = function initConnection(app) {
  mongoose.Promise = global.Promise;
  mongoose.connect(`mongodb://${mongoHost}:${mongoPort}/${mongoDb}`,
    { config: { autoIndex: app.get('env') !== 'production' } });
  return fixtures(app);
};
