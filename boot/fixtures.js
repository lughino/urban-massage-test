const { Character, User } = require('../models');

module.exports = function loadFixtures() {
  return Character.count().then(count => {
    if(count === 0) {
      const fix = require('./characters.json');
      return Character.insertMany(fix.characters)
        .then(() => console.log('Characters fixtures loaded'));
    }
  }).then(() => {
    return User.count();
  }).then(count => {
    if(count === 0) {
      const user = new User({
        name: 'Guest',
        surname: 'User',
        username: 'username',
        password: 'p4ssw0rd'
      });
      return user.save()
        .then(() => console.log('User fixtures loaded'));
    }
  });
};
