const express = require('express');
const Proto = require('uberproto');
const rest = require('./rest');

function createApplication() {
  const app = express();
  Proto.mixin(rest, app);
  app.init();

  return app;
}

Object.assign(createApplication, express);

module.exports = createApplication;
