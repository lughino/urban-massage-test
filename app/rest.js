const Proto = require('uberproto');

const methods = [
  'find',
  'get',
  'create',
  'update',
  'delete',
];

function wrapper(method, service) {
  return function middle(req, res, next) {
    if(!service[method])
      return next(new Error('Method not handled'));

    function cb(err, data) {
      if(err)
        return next(err);

      if(data)
        res.data = data;
      else
        res.status(204);

      return next();
    }
    return service[method](req, res, cb);
  };
}

function extractMiddlewares(method, service) {
  if(!service.middlewares)
    return [];

  return (service.middlewares.all || [])
    .concat(service.middlewares[method] || []);
}

function serviceWrapper(service) {
  function mix(...args) {
    const res = this._super(...args);
    const cb = args[args.length - 1];
    if(typeof cb === 'function' && res && typeof res.then === 'function')
      res.then(r => cb(null, r))
        .catch(e => cb(e));
    return res;
  }
  const wrappedService = {};
  methods.forEach(m => service[m] && (wrappedService[m] = mix));

  return wrappedService;
}

module.exports = {
  init() {
    Object.assign(this, {
      services: {},
    });
  },
  getService(name) {
    return this.services[name];
  },
  setService(location, instance) {
    Proto.mixin(serviceWrapper(instance), instance);
    this.services[location] = instance;
    const routes = this.route(location);
    const routesId = this.route(`${location}/:id`);

    routes.get(
      ...extractMiddlewares('find', instance),
      wrapper('find', instance));
    routes.post(
      ...extractMiddlewares('create', instance),
      wrapper('create', instance));
    routesId.get(
      ...extractMiddlewares('get', instance),
      wrapper('get', instance));
    routesId.put(
      ...extractMiddlewares('update', instance),
      wrapper('update', instance));
    routesId.delete(
      ...extractMiddlewares('delete', instance),
      wrapper('delete', instance));

    return this.services[location];
  },
  setup() {
    Object.keys(this.services).forEach(name => {
      const service = this.services[name];
      if(typeof service.setup === 'function') {
        service.setup(this, name);
      }
    });

    return this;
  },
  listen(...args) {
    const s = this._super.apply(this, args);
    this.setup(s);

    return s;
  },
};
