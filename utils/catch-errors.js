const HttpError = require('http-error-constructor');

module.exports = function catchErrorHandler(error) {
  if(error.name) {
    switch(error.name) {
      case 'UnauthorizedError':
        return Promise.reject(new HttpError.Unauthorized(error));
      case 'ValidationError':
      case 'ValidatorError':
      case 'CastError':
      case 'VersionError':
        return Promise.reject(new HttpError.BadRequest(error));
      case 'OverwriteModelError':
        return Promise.reject(new HttpError.Conflict(error));
      case 'MissingSchemaError':
      case 'DivergentArrayError':
        return Promise.reject(new HttpError[500](error));
      case 'MongoError':
        if(error.code === 11000 || error.code === 11001) {
          const match1 = error.message
            .match(/_?([a-zA-Z]*)_?\d?\s*dup key/i);
          const match2 = error.message
            .match(/\s*dup key:\s*\{\s*:\s*"?(\S+)"?\s*\}/i);

          const key = match1 ? match1[1] : 'path';
          let value = match2 ? match2[1] : 'value';

          if(value === 'null') {
            value = null;
          } else if(value === 'undefined') {
            value = undefined;
          }

          error.message = `${key}: ${value} already exists.`;
          error.errors = {
            [key]: value
          };

          return Promise.reject(new HttpError.Conflict(error));
        }
        return Promise.reject(new HttpError[500](error));
      default:
        return Promise.reject(new HttpError[500](error));
    }
  }

  return Promise.reject(error);
};
