module.exports = function errorFactory() {
  return function errorHandler(err, req, res, next) {
    const handlers = {
      json() {
        res.set('Content-Type', 'application/json');
        res.json(err);
      },
      // handle other mime types
    };
    const contentType = req.headers['content-type'] || '';
    res.status(err.statusCode || err.status);
    // handle response
    if(contentType.includes('json')) {
      handlers.json();
    } else {
      // fallback in json
      handlers.json();
    }
  };
};
