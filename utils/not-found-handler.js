const HttpError = require('http-error-constructor');

module.exports = function notFoundHandlerFactory() {
  return (req, res, next) =>
    next(new HttpError.NotFound('Source not found'));
};
