const jwt = require('express-jwt');
const jwtSign = require('jsonwebtoken');

const SECRET = 'uytvyttr676$"&SFT$W&W$"h98ujk';

module.exports = {
  jwtMiddleware: jwt({ secret: SECRET }),
  createToken(data, cb) {
    return jwtSign
      .sign(data, SECRET, { expiresIn: '1d' }, cb);
  }
};
