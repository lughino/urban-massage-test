module.exports = function formatMessageFactory() {
  return function formatMessage(req, res, next) {
    if(!res.data) {
      next();
    }

    return res.format({
      'application/json': () => {
        return res.json(res.data);
      },
    });
  };
};
