Urban Massage fullstack test
-----------------------------

This project requires `mongodb` as db dependency.

Clone the project:
```javascript
git clone https://lughino@bitbucket.org/lughino/urban-massage-test.git
cd urban-massage-test;
```

To install the application launch the following commands:

Install server dependencies:
```javascript
npm install
```
or
```javascript
yarn install
```

For semplicity I pushed a production build in the repository, so you don't need to install the client dependencies, but if you prefer you can install it with:
```javascript
cd client;
npm install
```
or
```javascript
cd client;
yarn install
```

Now run the servers:
In the root of project digit:
```javascript
NODE_ENV=production npm start
```
or
```javascript
NODE_ENV=production yarn start
```

That's it!

NOTES:
======

There are a lot of optimizations that can be applied to both client and server side, here list some of them:

Server:

* Caching with redis or similar
* Improve the automatic rest system for handle filters and custom methods
* Store revoked tokens
* Implement a complete login system (with permissions)
* Tuning db with indexing
* Use a global config system (like `node-config`)
* Dockerize server, db, and client

Client:

* Improve a graphic design
* Implement a robust caching system
* Some transitions would be great for a UX more comfortable
* Implement a complete login system
* Decoupling business logic
* Decoupling components
