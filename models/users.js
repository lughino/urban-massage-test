const mongoose = require('mongoose');
const crypto = require('crypto');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    maxlength: 24,
  },
  surname: {
    type: String,
    required: true,
    maxlength: 24,
  },
  username: {
    type: String,
    required: true,
    maxlength: 64,
    index: { unique: true },
  },
  password: {
    type: String,
    minlength: 6,
    required: true,
  },
  favourites: [{
    type: mongoose.Schema.ObjectId,
    ref: 'Character'
  }],
});

userSchema.statics.findByUsernameAndPassword = function findByUAndP(u, p) {
  const password = crypto.createHash('sha256').update(p).digest('base64');
  return this.findOne({ username: u, password });
};

userSchema.pre('save', function preSave(next) {
  if(!this.isModified('password')) return next();

  this.password = crypto.createHash('sha256')
    .update(this.password).digest('base64');
  return next();
});

module.exports = mongoose.model('User', userSchema);
