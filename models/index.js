const Character = require('./characters');
const User = require('./users');

module.exports = {
  Character,
  User,
};
